# stage-2-es6-for-everyone

## Simple step-by-step fighting game on pure JS

Choose two heroes by clicking on checkboxes below them, predict who would win and make your friends pay for wrong decisions, or pay yourself ;)

## Installation

`npm install`

`npm start`

open http://localhost:8080/

## Demo
![Demo gif](/demo/fightersDemo.gif)
