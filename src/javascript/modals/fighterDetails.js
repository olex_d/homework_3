import { showModal } from "./modal";
import {
  createAttackElement,
  createDefenceElement,
  createHealthElement,
  createModalBody,
  createNameElement,
} from "../helpers/modalContentHelper";
import {createImageElement} from "../helpers/imagesHelper";

export function showFighterDetailsModal(fighter) {
  const title = "Fighter info";
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createModalBody();

  const nameElement = createNameElement();
  const attackElement = createAttackElement();
  const defenceElement = createDefenceElement();
  const healthElement = createHealthElement();
  const imageElement = createImageElement(source);

  nameElement.innerText = name;
  attackElement.innerText = attack + " points";
  defenceElement.innerText = defense + " points";
  healthElement.innerText = health + " points";

  // show fighter name, attack, defense, health, image
  fighterDetails.append(imageElement);
  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenceElement);
  fighterDetails.append(healthElement);

  return fighterDetails;
}
