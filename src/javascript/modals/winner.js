import {
  createModalBody,
  createNameElement,
} from "../helpers/modalContentHelper";
import {showModal} from "./modal";
import {createImageElement} from "../helpers/imagesHelper";

export function showWinnerModal(fighter) {
  const title = "Winner";
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const winnerDetails = createModalBody();

  const winnerNameElement = createNameElement();
  winnerNameElement.classList.add("fire-text");
  const imageElement = createImageElement(source);

  winnerNameElement.innerText = name;

  winnerDetails.append(imageElement);
  winnerDetails.append(winnerNameElement);

  return winnerDetails;
}
