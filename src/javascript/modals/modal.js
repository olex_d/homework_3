import {
  createHeaderElement,
  createModalLayer,
  createModalRoot,
} from "../helpers/modalContentHelper";

export function showModal({ title, bodyElement }) {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement);

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById("root");
}

function createModal(title, bodyElement) {
  const layer = createModalLayer();
  const modalContainer = createModalRoot();
  const header = createHeaderElement(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}
