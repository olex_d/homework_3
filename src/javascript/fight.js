export function fight(firstFighter, secondFighter) {
  let winner = null;

  let initialFirstHealth = firstFighter.health;
  let initialSecondHealth = secondFighter.health;

  do {
    attack(firstFighter, secondFighter);
    if (secondFighter.health <= 0) winner = firstFighter;

    attack(secondFighter, firstFighter);
    if (firstFighter.health <= 0) winner = secondFighter;
  } while (winner == null);

  restoreHealth(firstFighter, initialFirstHealth);
  restoreHealth(secondFighter, initialSecondHealth);

  return winner;
}

function attack(firstFighter, secondFighter) {
  let receivedDamage = getDamage(firstFighter, secondFighter);
  secondFighter.health -= receivedDamage;
}

// cannot return values below zero. NaN and undefined also handled as 0
export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  return Math.max(0, damage) || 0;
}

export function getHitPower(fighter) {
  let criticalHitChance = randomChance();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  let dodgeChance = randomChance();
  return fighter.defense * dodgeChance;
}

function randomChance() {
  const min = 1;
  const max = 2;
  return Math.random() * (max - min) + min;
}

function restoreHealth(fighter, initialHealth) {
  fighter.health = initialHealth;
}
