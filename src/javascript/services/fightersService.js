import { callApi } from "../helpers/apiHelper";

export async function getFighters() {
  try {
    const fightersEndpoint = "fighters.json";
    return await callApi(fightersEndpoint, "GET");
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id) {
  try {
    const detailsEndpoint = `details/fighter/${id}.json`;
    return await callApi(detailsEndpoint, "GET");
  } catch (error) {
    throw error;
  }
}
