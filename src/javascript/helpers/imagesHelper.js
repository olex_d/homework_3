import {createElement} from "./domHelper";

export function createImageElement(source) {
    const attributes = { src: source };
    return createElement({
        tagName: "img",
        className: "fighter-image",
        attributes,
    });
}