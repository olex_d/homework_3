import { createElement } from "./domHelper";

export function createModalLayer() {
  return createElement({ tagName: "div", className: "modal-layer" });
}

export function createModalRoot() {
  return createElement({
    tagName: "div",
    className: "modal-root",
  });
}

export function createModalBody() {
  return createElement({
    tagName: "div",
    className: "modal-body",
  });
}

export function createHeaderElement(title) {
  const headerElement = createElement({
    tagName: "div",
    className: "modal-header",
  });
  const titleElement = createElement({ tagName: "span" });
  const closeButton = createElement({ tagName: "div", className: "close-btn" });

  titleElement.innerText = title;
  closeButton.innerText = "×";
  closeButton.addEventListener("click", hideModal);
  headerElement.append(title, closeButton);

  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName("modal-layer")[0];
  modal?.remove();
}

export function createNameElement() {
  return createElement({
    tagName: "div",
    className: "fighter-name",
  });
}

export function createAttackElement() {
  return createElement({
    tagName: "div",
    className: "fighter-attack",
  });
}

export function createDefenceElement() {
  return createElement({
    tagName: "div",
    className: "fighter-defence",
  });
}

export function createHealthElement() {
  return createElement({
    tagName: "div",
    className: "fighter-health",
  });
}
